package br.com.shepherd.web.rest;

import static br.com.shepherd.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import br.com.shepherd.ShepherdApp;
import br.com.shepherd.domain.PersonTreePath;
import br.com.shepherd.repository.PersonTreePathRepository;
import br.com.shepherd.web.rest.errors.ExceptionTranslator;

/**
 * Test class for the PersonTreePathResource REST controller.
 * @see PersonTreePathResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShepherdApp.class)
public class PersonTreePathResourceIntTest {

    private static final Integer DEFAULT_DEPTH = 1;
    private static final Integer UPDATED_DEPTH = 2;

    private static final ZonedDateTime DEFAULT_VALID_FROM =
        ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALID_FROM = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_VALID_TO =
        ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_VALID_TO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PersonTreePathRepository personTreePathRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPersonTreePathMockMvc;

    private PersonTreePath personTreePath;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if they test an entity which requires
     * the current entity.
     */
    public static PersonTreePath createEntity(EntityManager em) {
        PersonTreePath personTreePath = new PersonTreePath()
            .depth(DEFAULT_DEPTH)
            .validFrom(DEFAULT_VALID_FROM)
            .validTo(DEFAULT_VALID_TO);
        return personTreePath;
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonTreePathResource personTreePathResource = new PersonTreePathResource(personTreePathRepository);
        this.restPersonTreePathMockMvc = MockMvcBuilders.standaloneSetup(personTreePathResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        personTreePath = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonTreePath() throws Exception {
        int databaseSizeBeforeCreate = personTreePathRepository.findAll().size();

        // Create the PersonTreePath
        restPersonTreePathMockMvc.perform(post("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personTreePath)))
            .andExpect(status().isCreated());

        // Validate the PersonTreePath in the database
        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeCreate + 1);
        PersonTreePath testPersonTreePath = personTreePathList.get(personTreePathList.size() - 1);
        assertThat(testPersonTreePath.getDepth()).isEqualTo(DEFAULT_DEPTH);
        assertThat(testPersonTreePath.getValidFrom()).isEqualTo(DEFAULT_VALID_FROM);
        assertThat(testPersonTreePath.getValidTo()).isEqualTo(DEFAULT_VALID_TO);
    }

    @Test
    @Transactional
    public void createPersonTreePathWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personTreePathRepository.findAll().size();

        // Create the PersonTreePath with an existing ID
        personTreePath.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonTreePathMockMvc.perform(post("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personTreePath)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDepthIsRequired() throws Exception {
        int databaseSizeBeforeTest = personTreePathRepository.findAll().size();
        // set the field null
        personTreePath.setDepth(null);

        // Create the PersonTreePath, which fails.

        restPersonTreePathMockMvc.perform(post("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personTreePath)))
            .andExpect(status().isBadRequest());

        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValidFromIsRequired() throws Exception {
        int databaseSizeBeforeTest = personTreePathRepository.findAll().size();
        // set the field null
        personTreePath.setValidFrom(null);

        // Create the PersonTreePath, which fails.

        restPersonTreePathMockMvc.perform(post("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personTreePath)))
            .andExpect(status().isBadRequest());

        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPersonTreePaths() throws Exception {
        // Initialize the database
        personTreePathRepository.saveAndFlush(personTreePath);

        // Get all the personTreePathList
        restPersonTreePathMockMvc.perform(get("/api/person-tree-paths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personTreePath.getId().intValue())))
            .andExpect(jsonPath("$.[*].depth").value(hasItem(DEFAULT_DEPTH)))
            .andExpect(jsonPath("$.[*].validFrom").value(hasItem(sameInstant(DEFAULT_VALID_FROM))))
            .andExpect(jsonPath("$.[*].validTo").value(hasItem(sameInstant(DEFAULT_VALID_TO))));
    }

    @Test
    @Transactional
    public void getPersonTreePath() throws Exception {
        // Initialize the database
        personTreePathRepository.saveAndFlush(personTreePath);

        // Get the personTreePath
        restPersonTreePathMockMvc.perform(get("/api/person-tree-paths/{id}", personTreePath.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personTreePath.getId().intValue()))
            .andExpect(jsonPath("$.depth").value(DEFAULT_DEPTH))
            .andExpect(jsonPath("$.validFrom").value(sameInstant(DEFAULT_VALID_FROM)))
            .andExpect(jsonPath("$.validTo").value(sameInstant(DEFAULT_VALID_TO)));
    }

    @Test
    @Transactional
    public void getNonExistingPersonTreePath() throws Exception {
        // Get the personTreePath
        restPersonTreePathMockMvc.perform(get("/api/person-tree-paths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonTreePath() throws Exception {
        // Initialize the database
        personTreePathRepository.saveAndFlush(personTreePath);
        int databaseSizeBeforeUpdate = personTreePathRepository.findAll().size();

        // Update the personTreePath
        PersonTreePath updatedPersonTreePath = personTreePathRepository.findOne(personTreePath.getId());
        updatedPersonTreePath
            .depth(UPDATED_DEPTH)
            .validFrom(UPDATED_VALID_FROM)
            .validTo(UPDATED_VALID_TO);

        restPersonTreePathMockMvc.perform(put("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPersonTreePath)))
            .andExpect(status().isOk());

        // Validate the PersonTreePath in the database
        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeUpdate);
        PersonTreePath testPersonTreePath = personTreePathList.get(personTreePathList.size() - 1);
        assertThat(testPersonTreePath.getDepth()).isEqualTo(UPDATED_DEPTH);
        assertThat(testPersonTreePath.getValidFrom()).isEqualTo(UPDATED_VALID_FROM);
        assertThat(testPersonTreePath.getValidTo()).isEqualTo(UPDATED_VALID_TO);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonTreePath() throws Exception {
        int databaseSizeBeforeUpdate = personTreePathRepository.findAll().size();

        // Create the PersonTreePath

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPersonTreePathMockMvc.perform(put("/api/person-tree-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personTreePath)))
            .andExpect(status().isCreated());

        // Validate the PersonTreePath in the database
        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePersonTreePath() throws Exception {
        // Initialize the database
        personTreePathRepository.saveAndFlush(personTreePath);
        int databaseSizeBeforeDelete = personTreePathRepository.findAll().size();

        // Get the personTreePath
        restPersonTreePathMockMvc.perform(delete("/api/person-tree-paths/{id}", personTreePath.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PersonTreePath> personTreePathList = personTreePathRepository.findAll();
        assertThat(personTreePathList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonTreePath.class);
        PersonTreePath personTreePath1 = new PersonTreePath();
        personTreePath1.setId(1L);
        PersonTreePath personTreePath2 = new PersonTreePath();
        personTreePath2.setId(personTreePath1.getId());
        assertThat(personTreePath1).isEqualTo(personTreePath2);
        personTreePath2.setId(2L);
        assertThat(personTreePath1).isNotEqualTo(personTreePath2);
        personTreePath1.setId(null);
        assertThat(personTreePath1).isNotEqualTo(personTreePath2);
    }
}
