import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {OnInit} from "@angular/core";
import {DatePipe} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Rx";
import {DataUtils, DateUtils, EventManager} from "ng-jhipster";
import {ShepherdTestModule} from "../../../test.module";
import {MockActivatedRoute} from "../../../helpers/mock-route.service";
import {PersonTreePathDetailComponent} from "../../../../../../main/webapp/app/entities/person-tree-path/person-tree-path-detail.component";
import {PersonTreePathService} from "../../../../../../main/webapp/app/entities/person-tree-path/person-tree-path.service";
import {PersonTreePath} from "../../../../../../main/webapp/app/entities/person-tree-path/person-tree-path.model";

describe('Component Tests', () => {

    describe('PersonTreePath Management Detail Component', () => {
        let comp: PersonTreePathDetailComponent;
        let fixture: ComponentFixture<PersonTreePathDetailComponent>;
        let service: PersonTreePathService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ShepherdTestModule],
                declarations: [PersonTreePathDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PersonTreePathService,
                    EventManager
                ]
            }).overrideTemplate(PersonTreePathDetailComponent, '')
                .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PersonTreePathDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PersonTreePathService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new PersonTreePath(10)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.personTreePath).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
