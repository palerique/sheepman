package br.com.shepherd.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.shepherd.domain.PersonTreePath;
import br.com.shepherd.repository.PersonTreePathRepository;
import br.com.shepherd.web.rest.util.HeaderUtil;

/**
 * REST controller for managing PersonTreePath.
 */
@RestController
@RequestMapping("/api")
public class PersonTreePathResource {

    private static final String ENTITY_NAME = "personTreePath";
    private final Logger log = LoggerFactory.getLogger(PersonTreePathResource.class);
    private final PersonTreePathRepository personTreePathRepository;

    public PersonTreePathResource(PersonTreePathRepository personTreePathRepository) {
        this.personTreePathRepository = personTreePathRepository;
    }

    /**
     * POST  /person-tree-paths : Create a new personTreePath.
     * @param personTreePath the personTreePath to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personTreePath, or with status 400
     * (Bad Request) if the personTreePath has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/person-tree-paths")
    @Timed
    public ResponseEntity<PersonTreePath> createPersonTreePath(@Valid @RequestBody PersonTreePath personTreePath)
        throws URISyntaxException {
        log.debug("REST request to save PersonTreePath : {}", personTreePath);
        if (personTreePath.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil
                .createFailureAlert(ENTITY_NAME, "idexists", "A new personTreePath cannot already have an ID"))
                .body(null);
        }
        PersonTreePath result = personTreePathRepository.save(personTreePath);
        return ResponseEntity.created(new URI("/api/person-tree-paths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /person-tree-paths : Updates an existing personTreePath.
     * @param personTreePath the personTreePath to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personTreePath, or with status 400 (Bad
     * Request) if the personTreePath is not valid, or with status 500 (Internal Server Error) if the personTreePath
     * couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/person-tree-paths")
    @Timed
    public ResponseEntity<PersonTreePath> updatePersonTreePath(@Valid @RequestBody PersonTreePath personTreePath)
        throws URISyntaxException {
        log.debug("REST request to update PersonTreePath : {}", personTreePath);
        if (personTreePath.getId() == null) {
            return createPersonTreePath(personTreePath);
        }
        PersonTreePath result = personTreePathRepository.save(personTreePath);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personTreePath.getId().toString()))
            .body(result);
    }

    /**
     * GET  /person-tree-paths : get all the personTreePaths.
     * @return the ResponseEntity with status 200 (OK) and the list of personTreePaths in body
     */
    @GetMapping("/person-tree-paths")
    @Timed
    public List<PersonTreePath> getAllPersonTreePaths() {
        log.debug("REST request to get all PersonTreePaths");
        return personTreePathRepository.findAll();
    }

    /**
     * GET  /person-tree-paths/:id : get the "id" personTreePath.
     * @param id the id of the personTreePath to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personTreePath, or with status 404 (Not Found)
     */
    @GetMapping("/person-tree-paths/{id}")
    @Timed
    public ResponseEntity<PersonTreePath> getPersonTreePath(@PathVariable Long id) {
        log.debug("REST request to get PersonTreePath : {}", id);
        PersonTreePath personTreePath = personTreePathRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personTreePath));
    }

    /**
     * DELETE  /person-tree-paths/:id : delete the "id" personTreePath.
     * @param id the id of the personTreePath to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/person-tree-paths/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonTreePath(@PathVariable Long id) {
        log.debug("REST request to delete PersonTreePath : {}", id);
        personTreePathRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
