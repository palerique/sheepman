package br.com.shepherd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.shepherd.domain.PersonTreePath;


/**
 * Spring Data JPA repository for the PersonTreePath entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PersonTreePathRepository extends JpaRepository<PersonTreePath, Long> {

}
