package br.com.shepherd.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PersonTreePath.
 */
@Entity
@Table(name = "person_tree_path")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PersonTreePath implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "depth", nullable = false)
    private Integer depth;

    @NotNull
    @Column(name = "valid_from", nullable = false)
    private ZonedDateTime validFrom;

    @Column(name = "valid_to")
    private ZonedDateTime validTo;

    @ManyToOne
    private Person ancestor;

    @ManyToOne
    private Person descendant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public PersonTreePath depth(Integer depth) {
        this.depth = depth;
        return this;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public PersonTreePath validFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(ZonedDateTime validTo) {
        this.validTo = validTo;
    }

    public PersonTreePath validTo(ZonedDateTime validTo) {
        this.validTo = validTo;
        return this;
    }

    public Person getAncestor() {
        return ancestor;
    }

    public void setAncestor(Person person) {
        this.ancestor = person;
    }

    public PersonTreePath ancestor(Person person) {
        this.ancestor = person;
        return this;
    }

    public Person getDescendant() {
        return descendant;
    }

    public void setDescendant(Person person) {
        this.descendant = person;
    }

    public PersonTreePath descendant(Person person) {
        this.descendant = person;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonTreePath personTreePath = (PersonTreePath) o;
        if (personTreePath.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), personTreePath.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PersonTreePath{" +
            "id=" + getId() +
            ", depth='" + getDepth() + "'" +
            ", validFrom='" + getValidFrom() + "'" +
            ", validTo='" + getValidTo() + "'" +
            "}";
    }
}
