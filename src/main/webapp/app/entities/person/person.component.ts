import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Rx';
import {AlertService, EventManager} from 'ng-jhipster';

import {Person} from './person.model';
import {PersonService} from './person.service';
import {Principal, ResponseWrapper} from '../../shared';

@Component({
    selector: 'jhi-person',
    templateUrl: './person.component.html'
})
export class PersonComponent implements OnInit, OnDestroy {
    people: Person[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private personService: PersonService,
                private alertService: AlertService,
                private eventManager: EventManager,
                private principal: Principal) {
    }

    loadAll() {
        this.personService.query().subscribe(
            (res: ResponseWrapper) => {
                this.people = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPeople();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Person) {
        return item.id;
    }

    registerChangeInPeople() {
        this.eventSubscriber = this.eventManager.subscribe('personListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
