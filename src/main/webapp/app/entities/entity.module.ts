import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {ShepherdPersonModule} from './person/person.module';
import {ShepherdPersonTreePathModule} from './person-tree-path/person-tree-path.module';

/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ShepherdPersonModule,
        ShepherdPersonTreePathModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShepherdEntityModule {}
