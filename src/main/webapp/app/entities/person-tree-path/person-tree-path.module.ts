import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ShepherdSharedModule} from '../../shared';
import {
    PersonTreePathComponent,
    PersonTreePathDeleteDialogComponent,
    PersonTreePathDeletePopupComponent,
    PersonTreePathDetailComponent,
    PersonTreePathDialogComponent,
    PersonTreePathPopupComponent,
    personTreePathPopupRoute,
    PersonTreePathPopupService,
    personTreePathRoute,
    PersonTreePathService
} from './';

const ENTITY_STATES = [
    ...personTreePathRoute,
    ...personTreePathPopupRoute,
];

@NgModule({
    imports: [
        ShepherdSharedModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        PersonTreePathComponent,
        PersonTreePathDetailComponent,
        PersonTreePathDialogComponent,
        PersonTreePathDeleteDialogComponent,
        PersonTreePathPopupComponent,
        PersonTreePathDeletePopupComponent,
    ],
    entryComponents: [
        PersonTreePathComponent,
        PersonTreePathDialogComponent,
        PersonTreePathPopupComponent,
        PersonTreePathDeleteDialogComponent,
        PersonTreePathDeletePopupComponent,
    ],
    providers: [
        PersonTreePathService,
        PersonTreePathPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ShepherdPersonTreePathModule {
}
