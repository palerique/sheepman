import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Rx';
import {AlertService, EventManager} from 'ng-jhipster';

import {PersonTreePath} from './person-tree-path.model';
import {PersonTreePathService} from './person-tree-path.service';
import {Principal, ResponseWrapper} from '../../shared';

@Component({
    selector: 'jhi-person-tree-path',
    templateUrl: './person-tree-path.component.html'
})
export class PersonTreePathComponent implements OnInit, OnDestroy {
    personTreePaths: PersonTreePath[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(private personTreePathService: PersonTreePathService,
                private alertService: AlertService,
                private eventManager: EventManager,
                private principal: Principal) {
    }

    loadAll() {
        this.personTreePathService.query().subscribe(
            (res: ResponseWrapper) => {
                this.personTreePaths = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPersonTreePaths();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PersonTreePath) {
        return item.id;
    }

    registerChangeInPersonTreePaths() {
        this.eventSubscriber = this.eventManager.subscribe('personTreePathListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
