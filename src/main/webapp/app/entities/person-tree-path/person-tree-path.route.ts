import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';

import {PersonTreePathComponent} from './person-tree-path.component';
import {PersonTreePathDetailComponent} from './person-tree-path-detail.component';
import {PersonTreePathPopupComponent} from './person-tree-path-dialog.component';
import {PersonTreePathDeletePopupComponent} from './person-tree-path-delete-dialog.component';

export const personTreePathRoute: Routes = [
    {
        path: 'person-tree-path',
        component: PersonTreePathComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'shepherdApp.personTreePath.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'person-tree-path/:id',
        component: PersonTreePathDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'shepherdApp.personTreePath.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const personTreePathPopupRoute: Routes = [
    {
        path: 'person-tree-path-new',
        component: PersonTreePathPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'shepherdApp.personTreePath.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'person-tree-path/:id/edit',
        component: PersonTreePathPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'shepherdApp.personTreePath.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'person-tree-path/:id/delete',
        component: PersonTreePathDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'shepherdApp.personTreePath.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
