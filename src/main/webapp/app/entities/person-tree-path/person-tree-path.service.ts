import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {DateUtils} from 'ng-jhipster';

import {PersonTreePath} from './person-tree-path.model';
import {createRequestOption, ResponseWrapper} from '../../shared';

@Injectable()
export class PersonTreePathService {

    private resourceUrl = 'api/person-tree-paths';

    constructor(private http: Http, private dateUtils: DateUtils) {
    }

    create(personTreePath: PersonTreePath): Observable<PersonTreePath> {
        const copy = this.convert(personTreePath);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(personTreePath: PersonTreePath): Observable<PersonTreePath> {
        const copy = this.convert(personTreePath);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PersonTreePath> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.validFrom = this.dateUtils
            .convertDateTimeFromServer(entity.validFrom);
        entity.validTo = this.dateUtils
            .convertDateTimeFromServer(entity.validTo);
    }

    private convert(personTreePath: PersonTreePath): PersonTreePath {
        const copy: PersonTreePath = Object.assign({}, personTreePath);

        copy.validFrom = this.dateUtils.toDate(personTreePath.validFrom);

        copy.validTo = this.dateUtils.toDate(personTreePath.validTo);
        return copy;
    }
}
