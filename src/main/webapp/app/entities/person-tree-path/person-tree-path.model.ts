import {Person} from '../person';

export class PersonTreePath {
    constructor(public id?: number,
                public depth?: number,
                public validFrom?: any,
                public validTo?: any,
                public ancestor?: Person,
                public descendant?: Person, ) {
    }
}
