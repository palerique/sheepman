import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AlertService, EventManager} from 'ng-jhipster';

import {PersonTreePath} from './person-tree-path.model';
import {PersonTreePathPopupService} from './person-tree-path-popup.service';
import {PersonTreePathService} from './person-tree-path.service';

@Component({
    selector: 'jhi-person-tree-path-delete-dialog',
    templateUrl: './person-tree-path-delete-dialog.component.html'
})
export class PersonTreePathDeleteDialogComponent {

    personTreePath: PersonTreePath;

    constructor(private personTreePathService: PersonTreePathService,
                public activeModal: NgbActiveModal,
                private alertService: AlertService,
                private eventManager: EventManager) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.personTreePathService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'personTreePathListModification',
                content: 'Deleted an personTreePath'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('shepherdApp.personTreePath.deleted', {param: id}, null);
    }
}

@Component({
    selector: 'jhi-person-tree-path-delete-popup',
    template: ''
})
export class PersonTreePathDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private personTreePathPopupService: PersonTreePathPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.personTreePathPopupService
                .open(PersonTreePathDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
