import {Component, Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {DatePipe} from '@angular/common';
import {PersonTreePath} from './person-tree-path.model';
import {PersonTreePathService} from './person-tree-path.service';

@Injectable()
export class PersonTreePathPopupService {
    private isOpen = false;

    constructor(private datePipe: DatePipe,
                private modalService: NgbModal,
                private router: Router,
                private personTreePathService: PersonTreePathService) {
    }

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.personTreePathService.find(id).subscribe((personTreePath) => {
                personTreePath.validFrom = this.datePipe
                    .transform(personTreePath.validFrom, 'yyyy-MM-ddThh:mm');
                personTreePath.validTo = this.datePipe
                    .transform(personTreePath.validTo, 'yyyy-MM-ddThh:mm');
                this.personTreePathModalRef(component, personTreePath);
            });
        } else {
            return this.personTreePathModalRef(component, new PersonTreePath());
        }
    }

    personTreePathModalRef(component: Component, personTreePath: PersonTreePath): NgbModalRef {
        const modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.personTreePath = personTreePath;
        modalRef.result.then((result) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        });
        return modalRef;
    }
}
