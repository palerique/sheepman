import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Response} from '@angular/http';

import {Observable} from 'rxjs/Rx';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {AlertService, EventManager} from 'ng-jhipster';

import {PersonTreePath} from './person-tree-path.model';
import {PersonTreePathPopupService} from './person-tree-path-popup.service';
import {PersonTreePathService} from './person-tree-path.service';
import {Person, PersonService} from '../person';
import {ResponseWrapper} from '../../shared';

@Component({
    selector: 'jhi-person-tree-path-dialog',
    templateUrl: './person-tree-path-dialog.component.html'
})
export class PersonTreePathDialogComponent implements OnInit {

    personTreePath: PersonTreePath;
    authorities: any[];
    isSaving: boolean;

    people: Person[];

    constructor(public activeModal: NgbActiveModal,
                private alertService: AlertService,
                private personTreePathService: PersonTreePathService,
                private personService: PersonService,
                private eventManager: EventManager) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.personService.query()
            .subscribe((res: ResponseWrapper) => {
                this.people = res.json;
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.personTreePath.id !== undefined) {
            this.subscribeToSaveResponse(
                this.personTreePathService.update(this.personTreePath), false);
        } else {
            this.subscribeToSaveResponse(
                this.personTreePathService.create(this.personTreePath), true);
        }
    }

    trackPersonById(index: number, item: Person) {
        return item.id;
    }

    private subscribeToSaveResponse(result: Observable<PersonTreePath>, isCreated: boolean) {
        result.subscribe((res: PersonTreePath) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PersonTreePath, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'shepherdApp.personTreePath.created'
                : 'shepherdApp.personTreePath.updated',
            {param: result.id}, null);

        this.eventManager.broadcast({name: 'personTreePathListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-person-tree-path-popup',
    template: ''
})
export class PersonTreePathPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private personTreePathPopupService: PersonTreePathPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if (params['id']) {
                this.modalRef = this.personTreePathPopupService
                    .open(PersonTreePathDialogComponent, params['id']);
            } else {
                this.modalRef = this.personTreePathPopupService
                    .open(PersonTreePathDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
