export * from './person-tree-path.model';
export * from './person-tree-path-popup.service';
export * from './person-tree-path.service';
export * from './person-tree-path-dialog.component';
export * from './person-tree-path-delete-dialog.component';
export * from './person-tree-path-detail.component';
export * from './person-tree-path.component';
export * from './person-tree-path.route';
