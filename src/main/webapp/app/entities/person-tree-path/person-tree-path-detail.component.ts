import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {EventManager} from 'ng-jhipster';

import {PersonTreePath} from './person-tree-path.model';
import {PersonTreePathService} from './person-tree-path.service';

@Component({
    selector: 'jhi-person-tree-path-detail',
    templateUrl: './person-tree-path-detail.component.html'
})
export class PersonTreePathDetailComponent implements OnInit, OnDestroy {

    personTreePath: PersonTreePath;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(private eventManager: EventManager,
                private personTreePathService: PersonTreePathService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPersonTreePaths();
    }

    load(id) {
        this.personTreePathService.find(id).subscribe((personTreePath) => {
            this.personTreePath = personTreePath;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPersonTreePaths() {
        this.eventSubscriber = this.eventManager.subscribe(
            'personTreePathListModification',
            (response) => this.load(this.personTreePath.id)
        );
    }
}
